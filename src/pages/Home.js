import Banner from '../components/Banner.js'
import Highlight from '../components/Highlight';
import Courses from './Courses'

function Home(){
     
     return(
          <>
               <Banner />
               <Highlight />
          
          </>
     )
}

export default Home;