// Activity 53 NOT FOUND PAGE
import { Button } from "react-bootstrap"
import notfoundsad from "../images/notfoundsad.jpg"
import {Link} from 'react-router-dom'

function NotFound(){
     return(
          <div id="notfound">
              <div>
                    <h1 className="notfoundheader">&nbsp;404 Error</h1>
                    <h1>Uh-oh..</h1>
                    <h4>The page you were looking for does not exist.</h4>
               </div> 
               <div>
                    <img src={notfoundsad} className=""></img>
               </div>
               
               <Button as= {Link} to='/' variant="dark" style={{width: 150}} className="mt-5"><Link className="link"to='/'>Back To Home</Link></Button>          
          </div>
     )
}



export default NotFound;
