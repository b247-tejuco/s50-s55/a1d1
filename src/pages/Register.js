import { Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2'



function Register(){
     const {user, setUser} = useContext(UserContext)
     // state hooks to store values of the input fields
     const [email, setEmail] = useState("");
     const [password1, setPassword1] = useState("");
     const [password2, setPassword2] = useState("");
     const [firstName, setFirstName] = useState("");
     const [lastName, setLastName] = useState("");
     const [mobileNumber, setMobileNumber] = useState("");
     const navigate= useNavigate()
    

     // det if submit btn is enabled or not
     const [isActive, setIsActive] = useState(true)

     // Validation to enable submit button when all fields are populated and both passwords match.
     /* function registerUser(e){
          e.preventDefault()
          setEmail("");
          setPassword1("");
          setPassword2("");

          alert("Thank you for registering!")
     } */
     useEffect(() => {

          if((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "" && mobileNumber !== "" && ( password1 === password2) && (password1.length > 7) && (mobileNumber.length >= 11) && (email.length >= 10)))
          {
               setIsActive(true);
          } else {
               setIsActive(false);
          }

     }, [email, password1, password2, firstName, lastName, mobileNumber])

     function registerUser(e){
          e.preventDefault()


          // checking duplicate email
          fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
               method: 'POST',
               headers: {
                    'Content-type': 'application/json'
               },
               body: JSON.stringify({
                    email:email
               })

          })
          .then(res => res.json())
          .then(data => {
               if(data === true){
                    Swal.fire({
                         title: "Registration failed!",
                         icon: "error",
                         text: "Email already exists!"
                    })
               } else {
                    
                    // register user . push to database
                    next()
               
               
               }
          })
     }

     function next(){

                    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: 'POST',
                    headers: {
                         'Content-type': 'application/json'
                    },
                    body: JSON.stringify({

                         email:email,
                         password: password1,
                         mobileNo: mobileNumber,
                         firstName: firstName,
                         lastName: lastName
                     })
                 })
                 .then(res => res.json())
                 .then(data => {
                    if(data === true){
                         Swal.fire({
                              title: "Registration successful!",
                              icon: "success",
                              text: "You have successfully registered! You can log in now"
                         })
                    
                    navigate('/login');

                    } else {
                         Swal.fire({
                              title: "Registration failed!",
                              icon: "error",
                              text: "Oh no, an error has occured. Please try again later."
                         })
                    }
                 })
                 
          }


     return(
          
          (user.id !== null)
          ?
          
          <Navigate to ="/courses"/>
          :
          
          
          <div className='logcont'>
          <Form className='regform'>
               <h1 style={{ color: 'steelblue', textAlign: 'center', padding: 15}}>Register</h1>

               <Form.Group>
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                    type="text"
                    value={firstName}
                    onChange= {e => setFirstName(e.target.value)}
                    required />
               </Form.Group>

               <Form.Group>
                    <Form.Label className='pt-2'>Last Name</Form.Label>
                    <Form.Control
                    value={lastName}
                    type="text"
                    onChange= {e => setLastName(e.target.value)}
                    required />
               </Form.Group>
               <Form.Group controlId="email">
                    <Form.Label className='pt-2'>Email Address</Form.Label>
                    <Form.Control 
                    type="email" 
                    placeholder="name@example.com" 
                    value= {email}
                    onChange= {e => setEmail(e.target.value)}
                    required
                    />
                    <Form.Text className='text-muted'>
                         We'll never share your email with anyone else.
                    </Form.Text>
               </Form.Group>

               <Form.Group>
                    <Form.Label className='pt-2'>Mobile Number</Form.Label>
                    <Form.Control 
                    type="number"
                    value={mobileNumber}
                    placeholder="+63 900 000 0000"
                    onChange= {e => setMobileNumber(e.target.value)}
                    required
                    />
               </Form.Group>

               <Form.Group controlId="password1">
                    <Form.Label className='pt-2'>Password</Form.Label>
                    <Form.Control 
                    type="password" 
                    placeholder="Enter a minimum of 8 characters"
                    value= {password1}
                    onChange= {e => setPassword1(e.target.value)}
                    required/>
               </Form.Group>

               <Form.Group controlId="password2">
                    <Form.Label className='pt-2'>Confirm Password</Form.Label>
                    <Form.Control 
                    type="password" 
                    required
                    value= {password2}
                    onChange= {e => setPassword2(e.target.value)}
                    />
               </Form.Group>

               { isActive ?
                              <Button onClick={registerUser} variant="info" type="submit" id="submitBtn">Submit</Button>
                    
                         :
                    
                               <Button variant="secondary" type="submit" id="submitBtn" disabled >Submit</Button>
               }    
          </Form>
          </div>
     )

}


export default Register;
