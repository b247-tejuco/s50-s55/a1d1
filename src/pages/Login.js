// s52 Activity
import { Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



     function Login(props){

          const {user, setUser} = useContext(UserContext)
          // state hooks to store values of the input fields
          const [email, setEmail] = useState("");
          const [password, setPassword] = useState("");
     
          // det if submit btn is enabled or not
          const [isActive, setIsActive] = useState(false)
     
          // Validation to enable submit button when all fields are populated and both passwords match.

          // hook return a func that lets u navigate to components

          const navigate = useNavigate();


          function loginUser(e){
               e.preventDefault();
               fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
                    method: 'POST',
                    headers: {
                         'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                         email: email,
                         password: password
                    })
               })
               .then(res => res.json())
               .then(data=>{

                    // we will receive either a token or an error response
                    console.log(data);
                    
                    // if no user info is found, the 'access' property will not be available and will return undefined

                    // using the typeof operator will return a string of data type of the variable/expression it preceeds which is why the valuie being compared is in a string type data type

                    if(typeof data.access !== "undefined"){

                         // the JWT will be used to retrieve user information across the whole frontend application and storing it in the localStorage will allow ease of access to the user's information

                         localStorage.setItem('token', data.access)
                         retrieveUserDetails(data.access)

                         Swal.fire({
                              title: 'Logged in Successful',
                              icon: 'success',
                              text: 'Welcome to Zuitt!'
                         })
                         
                    } else {
                         Swal.fire({
                              title: "Authentication Failed!",
                              icon: "error",
                              text: "Please, check your login details and try again."
                         })
                    }
               })

               // sets the email of the authhenticated user in the local storage
               // localStorage.setItem("email", email);


               //syntax localStorage.setItem('propertyName', value)
               // setUser( { email: localStorage.getItem('email') } );
               setEmail("");
               setPassword("");

               // 
               // navigate('/')
     
               // alert("Logged in successfully")
          };

          const retrieveUserDetails = (token) => {
               // the token will be sent as part of the req's header info
               // we put bearer in front of the token to follow implementation standards for JWT
               fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                    headers: {
                         Authorization: `Bearer ${token}`
                    }
               })
               .then(res => res.json())
               .then(data => {
                    console.log(data);
                    
                    // global user state for validation across the whole app
                    // changes the global 'user' state to store the 'id' and the 'isAdmin' property of the user which willbe used for valdiation across the whole application.
                    setUser({
                         id: data._id,
                         isAdmin: data.isAdmin
                    })
               })
          }




          useEffect(() => {
     
               if(email !== "" && password !== ""){
                    setIsActive(true);
               } else {
                    setIsActive(false);
               }
     
          }, [email, password])

     return(
          
     
     (user.id !== null)
     ?
       <Navigate to ="/courses"/>
     :

     <div className="logcont">
          <Form className='logform'>
               <h1 style={{ color: 'steelblue', textAlign: 'center', padding: 15}}>Login</h1>
          <div>
               <Form.Group controlId="email">
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control 
                    type="email" 
                    placeholder="Enter email here" 
                    value= {email}
                    onChange= {e => setEmail(e.target.value)}
                    required
                    />
               </Form.Group>

               <Form.Group controlId="password">
                    <Form.Label className='pt-2'>Password</Form.Label>
                    <Form.Control 
                    type="password" 
                    placeholder="Enter password here"
                    value= {password}
                    onChange= {e => setPassword(e.target.value)}
                    required/>
               </Form.Group>
          </div>

               

               { isActive ?
                              <Button onClick={loginUser} variant="info" type="submit" id="submitBtn">Submit</Button>
                    
                         :
                    
                               <Button variant="secondary" type="submit" id="submitBtn" disabled >Submit</Button>
               }    
          </Form>
     </div>
     )

}


export default Login