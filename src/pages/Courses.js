/* import coursesData from "../data/coursesData"; */
import CourseCard from "../components/CourseCard";
import {useEffect, useState} from 'react'
import Loading from '../components/Loading.js'

function Courses(){

     // const courses = coursesData.map(course => {
     //      return(
     //           <CourseCard key = {course.id} courseData = {course} />
     //           /* key + map in REACT displays whole list from imported data as long
     //           as each have unique id which is almost always going to be put inside our 'key' */
     //           /* review study map, keys */
     //      )
     // })

     // state that will be used to store the courses retrieved from the database
     const [ courses, setCourses] = useState([]);
     const [isLoading, setIsLoading] = useState(true)


     // retrieves the courses from the database upon initital render of the 'Courses' component.

     useEffect(() => {
          fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
          .then(res => res.json())
          .then(data => {
               setCourses(data.map(course => {
                    return(
                         <CourseCard key={course._id} coursedata={course}/>
                    )
               }))
               
               setIsLoading(false)
          })
     }, [])

     return (
          (isLoading)?
          <Loading />
          :
          
          <>
          <h1 className="mt-5 mb-3">Courses</h1>
          {courses}
          </>
     )
}


export default Courses;