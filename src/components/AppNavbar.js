import { useState, useContext } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom'
import zuittlogo from "../images/zuittlogo.png"


function AppNavbar() {

  // state to store the user information stored in the login page.
  // the getItem() method is used when passed a key name, will return
  // that key's value, or null if the kye does not exist, in the given storage object.
  // const [user, setUser] = useState(localStorage.getItem('email'))

  const { user } = useContext(UserContext);
  

  
  return (
    <Navbar bg="secondary" expand="lg">
        <Navbar.Brand><Link to="/"><img id="logo" src={zuittlogo}/></Link></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="navlist mx-auto">
            <Link className="link" to="/">Home</Link>
            <Link className="link" to="/courses">Courses</Link>
            { 
            
            (user.id !== null)  ?

            <Link className="link"to="/logout">Logout</Link>

                    :

            <>
            <Link className="link" to="/register">Register</Link>
            <Link className="link" to="/login">Login</Link>
            </>

            }

          </Nav>
        </Navbar.Collapse>  
    </Navbar>
  );
}


export default AppNavbar;