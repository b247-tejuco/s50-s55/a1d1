
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types'
import { useState, useEffect } from 'react';



// Activity 50 -- Make a Course Card
function CourseCard({coursedata}){
/*     console.log(props.courseData.name) */

const { name, description, price, id } = coursedata

// Activity 51 -- Alert If Reached Max Slot (30/30) enrollees
const [count, setCount] = useState(0);
const [seats, setSeats] = useState(30);

const enroll = () => {
    if(count < 30){
        setCount( count + 1);
        setSeats( seats - 1);
    } else {
        alert('No more slots left')
       
    }
}

/* useEffect(()=>{
    if(seats === 0){
        document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
    }

}, [seats]) */

    
    // curly braces = javascript
    // props ≈ parameters, can be passed around from one component to another
    // props drilling

     return(
           <Card className='coursecard p-3'>
               <h5>{name}</h5>
               <h6>Description</h6>
               <p>{description}</p>
               <br />
               <h6 className="courseprice pt-2">Price</h6>
               <p>&#x20B1;{price}</p>
               <h6>Enrollees</h6>
               <p>{count} Enrollees</p>
               <h6>Seats</h6>
               <p>{seats} Seats</p>
               <Button className="coursebtn" onClick={enroll} id={`btn-enroll-${id}`}> Enroll</Button>
           </Card>
     )
}


// Check if the CourseCard component is getting the correct prop types
// PropTypes are used for validation information passed to a component and is a tool
// normally used to help developers ensure that the correct information is passed from one  components to the next.

CourseCard.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}




export default CourseCard;