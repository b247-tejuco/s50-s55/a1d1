
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom';




function CourseCard({coursedata}){


const { name, description, price, _id } = coursedata



     return(
          
          <Card className='coursecard p-3'>
               <h5>{name}</h5>
               <h6>Description</h6>
               <p>{description}</p>
               <br />
               <h6 className="courseprice pt-2">Price</h6>
               <p>&#x20B1;{price}</p>
               <Button className="coursebtn" as={Link} to={`/courses/${_id}`}> Details</Button>
           </Card>

     )
}


// Check if the CourseCard component is getting the correct prop types
// PropTypes are used for validation information passed to a component and is a tool
// normally used to help developers ensure that the correct information is passed from one  components to the next.

CourseCard.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}




export default CourseCard;