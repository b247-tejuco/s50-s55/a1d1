import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import {Link} from 'react-router-dom'

function Banner(){
     return (
          <Col className='m-3 p-5'>
               <Row>
                    <h1>Zuitt Coding Bootcamp</h1>
                    <p>Opportunities for everyone, everywhere</p>
               </Row>
               <Button as={Link} to="/courses" id="bannerbtn" variant="info" className="text-white"><strong>Enroll now!</strong></Button>
          </Col>
     )
}


export default Banner;