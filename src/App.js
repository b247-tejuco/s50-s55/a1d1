import Container from "react-bootstrap/Container";
import AppNavbar from './components/AppNavbar.js'
import Home from './pages/Home.js'
import Courses from './pages/Courses.js'
import Register from "./pages/Register.js";
import Login from "./pages/Login.js"
import Logout from "./pages/Logout.js"
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import NotFound from "./pages/NotFound.js";
import { useState, useEffect } from 'react';
import { UserProvider } from "./UserContext.js";
import CourseView from './components/CourseView.js'

function App() {
    const [user, setUser] = useState({
        // allows us to store and access the properties in the 'user Id' and the 'isAdmin' data
        id: null,
        isAdmin: null
    })
    // const [user, setUser] = useState({email: localStorage.getItem("email")});
    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data._id)
            // user is logged in
            if(typeof data._id !== "undefined"){
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            
            // user is logged out
            } else {
                setUser({
                    id: null,
                    isAdmin: null
                })
            }
            
        })
    }, []);

    

  return (
      <>
      <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
         <AppNavbar/>
            <Container>
                <Routes>
                    <Route path="/" element={<Home/>}/>     
                    <Route path="/courses/" element={<Courses/>}/>
                    {/* got the input for the params :courseId through the fetch+map done in Courses Page, which was also got passed to CourseCard, which is why :courseId is already defined at this point and we have pathing whenever we click Details button for CourseView */}
                    <Route path="/courses/:courseId" element={<CourseView/>}/>
                    <Route path="/register" element={<Register/>}/>
                    <Route path="/login" element={<Login />}/>
                    <Route path="/logout" element={<Logout />}/>
                    <Route path="*" element={<NotFound />}/>
                </Routes>
            </Container>
      </Router>
      </UserProvider>
      </>
  );
}

export default App;