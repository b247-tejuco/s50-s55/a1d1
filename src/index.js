import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.js';
import './index.css';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';



// JSX Simple Example
// const user= {
//   firstName: 'Jane',
//   lastName: 'Smith'
// };

// const formatName=(/* x */)=>{
//   return (
//     `${user.firstName} ${user.lastName}`
//     /* prompt(x) */
//   )
// }
// const element= <h1>Hello, {formatName(user)}</h1> 

// JSX allows us to create HTML elements and at the same time allows us to apply JS codes to these elemeents making it easy to write both HTML and Javascript code in a single file.

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);

//React Strictmode returns typo errors.