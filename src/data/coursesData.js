const coursesData= [
     {
          id: "wdc001",
          name: "PHP - Laravel",
          description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae sit assumenda dolores, reiciendis optio quidem esse qui distinctio? Pariatur saepe nisi quod sunt excepturi aliquid beatae assumenda consectetur dolorem nihil?",
          price: 45_000,
          onOffer: true

     },
     {
          id: "wdc002",
          name: "Python - Django",
          description: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
          price: 50_000,
          onOffer: true

     },
     {
          id: "wdc003",
          name: "Java - Springboot",
          description: "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",
          price: 55_000,
          onOffer: true

     },
     {
          id: "wdc004",
          name: "Ruby on Rails",
          description: "Congue mauris rhoncus aenean vel elit. Pharetra pharetra massa massa ultricies mi quis hendrerit dolor magna. Accumsan in nisl nisi scelerisque eu ultrices vitae. Aliquet risus feugiat in ante metus dictum at tempor. Id neque aliquam vestibulum morbi blandit cursus risus at ultrices. Hendrerit gravida rutrum quisque non tellus orci ac. In mollis nunc sed id. At imperdiet dui accumsan sit amet. Facilisis gravida neque convallis a cras semper auctor neque vitae. Sed arcu non odio euismod lacinia. Vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Cursus eget nunc scelerisque viverra mauris. Vulputate ut pharetra sit amet aliquam id diam.",
          price: 60_000,
          onOffer: false

     }



];

export default coursesData;